//app.js
App({
  onLaunch: function () {
    this.getPhoneInfo()
  },

  globalData: {
    userInfo: null,
    phoneInfo:{},
    HTTP: "https://www.llemon.net",
    token: '',
  },

  getPhoneInfo() {
    var that = this 
    this.globalData.phoneInfo.android6 = false
    this.globalData.phoneInfo.android = false
    wx.getSystemInfo({
      success: function (res) {
        // console.log('系统信息res:', res)
        that.globalData.phoneInfo.screenWidth = res.screenWidth
        that.globalData.phoneInfo.screenHeight = res.screenHeight
        that.globalData.phoneInfo.windowHeight = res.windowHeight
        that.globalData.phoneInfo.pixelRatio = res.pixelRatio
      },
      fail: function (res) {
        //console.log('获取手机信息失败', res)
        setTimeout(function () {
          that.getPhoneInfo()
        }, 3000);
      }
    })
  },
})