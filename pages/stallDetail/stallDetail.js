// pages/stallDetail/stallDetail.js
var network = require("../../utils/network.js")

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // banner
    duration: 2000,
    indicatorDots: true,
    autoplay: true,
    interval: 3000,
    banner: [],
    imageHeight: 0,
    oid: '',
    stall: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.data.oid = options.oid
    this.getBanners()
    this.getStallDetail()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  getBanners() {
    var that = this;
    network.request({
      url: '/sailclient/banner/search',
      data: {
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded' 
      },
      method: 'POST',
      dataType: 'json',
      responseType: 'text',
      success: function (res) {
        console.log('轮播图',res)
        // var height = app.globalData.phoneInfo.screenWidth * image.height / image.width
        var height = 150
        that.setData({
          imageHeight: height,
          banner: res.data.data
        })
      },
      fail: function (res) { },
      complete: function (res) { },
    })
  },

  getStallDetail() {
    var that = this;
    network.request({
      url: '/sailclient/stall/read/' + this.data.oid,
      data: {
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded' 
      },
      method: 'POST',
      dataType: 'json',
      responseType: 'text',
      success: function (res) {
        console.log('摊点详情', res)
        that.setData({
          stall: res.data.data
        })
      },
      fail: function (res) { },
      complete: function (res) { },
    })
  },

  navToStall() {
    const latitude = this.data.stall.latitude
    const longitude = this.data.stall.longitude
    wx.openLocation({
      latitude,
      longitude,
      scale: 18
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})