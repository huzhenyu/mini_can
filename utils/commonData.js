var shareText = {
  '1': "愿以此功德，庄严佛净土。上报四重恩，下济三途苦。若有见闻者，悉发菩提心。尽此一报身，同生极乐国。", 
  '2': "愿生西方净土中，九品莲花为父母。花开见佛悟无生，不退菩萨为伴侣。 ", 
  '3': "文殊师利勇猛智，普贤慧行亦复然。我今回向诸善根，随彼一切常修学。三世诸佛所称叹，如是最胜诸大愿。我今回向诸善根，为得普贤殊胜行。",
}

var RealnameTip = 'RealnameTip'
var HiddenEndAct = 'HiddenEndAct'
var AutoSaveAct = 'AutoSaveAct'
var AutoMusic = 'AutoMusic'

var getCommon = function (type) {
  var result = wx.getStorageSync(type)
  if (result == undefined) {
    return true
  } else {
    return result;
  }
}

var setCommon = function (type, value) {
  wx.setStorageSync(type, value)
}

module.exports = {
  RealnameTip,
  HiddenEndAct,
  AutoSaveAct,
  AutoMusic,
  shareText,
  getCommon,
  setCommon
}