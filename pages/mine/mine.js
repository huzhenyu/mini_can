// pages/mine/mine.js
var network = require("../../utils/network.js")
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userinfo:{},
    user:{},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      userinfo: getApp().globalData.userInfo,
      isHidden: app.globalData.isHidden
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (app.globalData.userInfo != null) {
      this.setData({
        userinfo: app.globalData.userInfo
      })
    }

    this.uploadUserinfo()
  },

  getUserInfo() {
    var that = this;
    network.request({
      url: '/api/user/view',
      data: {},
      header: {},
      method: 'GET',
      dataType: 'json',
      responseType: 'text',
      success: function (res) {
        that.setData({
          user: res.data.data
        })
        app.globalData.user_id = that.data.user.id
        app.globalData.realname = that.data.user.realname
        that.getSummaryData()
      },
      fail: function (res) { },
      complete: function (res) { },
    })
  },

  uploadUserinfo() {
    var that = this
    wx.getUserInfo({
      success: res => {
        var userInfo = res.userInfo
        that.setData({
          userinfo: userInfo
        })
      },
      fail: res => {
        that.gotoUserInfoPage()
      }
    })
  },

  gotoUserInfoPage() {
    wx.navigateTo({
      url: '/pages/userInfo/userInfo',
      success: function (res) { },
      fail: function (res) { },
      complete: function (res) { },
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})