function ChannelInfo(channel) {
  this.channel = channel
  this.lessonType = 0
  this.lessonCode =  ''
  this.isSetting = false
  this.title = (channel != 6) ? '点击添加功课' : "顶礼"
  this.icon = channel
  this.today_num = 0
  this.total_num = 0
  this.device_index = channel
  this.user_lesson_id = 0
  this.lesson_id = 0
  this.id = 0
  this.editing = false
  this.content = ''
  this.data_type = 0
  if (channel == 6) {
    this.lessonType = 1
    this.lesson_id = 1
    this.content = '自大圣境五台山，\n文殊加持入心间，\n祈祷晋美彭措躇，\n证悟意传求加持。'
    this.data_type = 1
    this.id = 1
  }
}

function Device() {
  this.name = ''
  this.advName = ''
  this.macID = ''
  this.softVersion = 0
  this.hasNewVersion = false
  this.connected = false
  this.battery = 0
}

function StepInfo(date, step) {
  this.date = date
  this.num = step
}

module.exports = {
  ChannelInfo,
  Device,
  StepInfo
}